---
title: "Matthew Graybosch"
subtitle: |
  I'm an author, a software developer, a long-haired metalhead, and 
  an incorrigible bookworm with delusions of erudition.
lang: "en"
keywords:
  - author
  - science fiction
  - fantasy
  - cyberpunk
  - gonzo
  - writing
  - heavy metal
  - metalhead
  - Unix
  - Linux
  - OpenBSD
  - rants
  - homepage
  - Starbreaker
---

# Here's Why You Should Stick Around

I write gonzo cyberpunk fantasy on my lunch breaks. My novels include *Without
Bloodshed* and *Silent Clarion*. I'm supposed to be working on *Shattered
Guardian*, and I might actually finish it before Ragnarok.

# More About Me

If the introduction above wasn't enough, here's some more for you.

* [My author bio](/about/bio/)
- [Press Kit](/about/press/)
- [Why I Write](/about/manifesto/)
- [How to Reach Me](/contact/)

# Starbreaker

Looking for a gonzo cyberpunk fantasy saga inspired by progressive rock and
heavy metal? Welcome to *Starbreaker*, where all-too-human androids and
swashbuckling soprano catgirls expose corruption and fight demons from outer
space.

Start reading using the links below, [unless you'd like to know more](/starbreaker/).

## Novels

* [Shattered Guardian](/starbreaker/shattered-guardian/) &mdash; Part II of Starbreaker (WIP)
* [Silent Clarion](/starbreaker/silent-clarion/) &mdash; a Starbreaker novel (2016)
* [Without Bloodshed](/starbreaker/without-bloodshed/) &mdash; Part I of Starbreaker (2013)

# Diary of a Madman

I have a blog of sorts. I basically make a new file, and start a new heading for
each day. At the end of the month I rename the file and start again. 

This is how we blogged in the 1990s.

* [This Month's Entries](/news/)
- [Archive](/news/archive/)

# More Good Stuff

I'm not just a sci-fi/fantasy/speculative fiction/etc author. Here are pages
related to my other interests.

* [Bad Writing Advice](/bad-writing-advice/) 
* [Metal Appreciation](/metal-appreciation/) (music)
* [Ex Libris](/ex-libris/) (books)
* [Hardcore Casual](/hardcore-casual/) (games)
* [Two Thumbs Down](/two-thumbs-down/) (movies/video/anime)
* [Mewnix](/mewnix/) (Unix, Linux, and FOSS)
* [The Rebel Branch Initiate's Guide to The Alchemists' Council](/rebel-branch-guide/)
* [Pretties for You](/pretties-for-you) (art & photography)
* [Considered Harmful](/considered-harmful/) (rants & other unreasonable opinions)
* [/dev/random/](/dev-random/) (everything else)

# Thanks for Visiting

Kudos, questions, and other comments to [public@matthewgraybosch.com](mailto:public@matthewgraybosch.com). Flames to /dev/null.
