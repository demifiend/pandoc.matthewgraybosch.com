---
title: "How to Contact Me"
subtitle: |
  Want to talk? No problem. Here's how to do reach me by email or on social media.
lang: "en"
keywords:
  - contact
  - email
  - social media
  - Mastodon
  - Reddit
  - Google+
breadcrumbs:
  - name: Home
    url: /
---

# Use Email First

If you need to get in touch, [email is your best bet](mailto:public@matthewgraybosch.com). Yes, it's old-school. In case you couldn't tell from this web-site, I dig old-school.

# Try Social Media

If email is too old-fashioned for you, you can find me on [Mastodon](https://octodon.social/@starbreaker), or in one 
of the following walled gardens.

* [Reddit](https://www.reddit.com/user/mgraybosch)
* [Google+](ttps://plus.google.com/+MatthewGraybosch)



# Other Methods

If you play games on the PS3 and PS4 consoles, my SEN handle is [EddieVanHelsing](https://psnprofiles.com/EddieVanHelsing). Don't 
count on being able to reach me here, however, because I keep my
privacy settings locked down tight.

If you have a problem with my website, [please open an issue on Github](https://github.com/matthewgraybosch/pandoc.matthewgraybosch.com).

# Note to Tech Tecruiters

Don't contact me unless you have *personally* helped me find work in 
the past. I'm not looking for work, and therefore am not interested
anything you might say to me.
