# pandoc.matthewgraybosch.com

This is new version of my website built with Pandoc and a very small shell
script.

# Instructions

Use ```imageprep.sh``` to resize and optimize images after placing copies in
images/incoming.

Use ```build.sh``` to build and deploy the site.

# Licenses

Code is available under the 3-clause BSD license.

Original content is available under the Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International license.
