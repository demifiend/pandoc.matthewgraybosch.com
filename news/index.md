--- 
title: "Diary of a Madman" 
subtitle: | 
  Watch this page for updates, news, short notes, and random musings. 
  This is the closest I'm willing to get to writing a blog. 
lang: "en"
keywords:
  - Matthew Graybosch
  - blog
  - journal
  - news
breadcrumbs:
  - name: Home
    url: /
---

All entries are in reverse chronological order, and time-stamped as necessary.
When I think this page has gotten too long I'll archive it and start fresh.

# 23 October 2017

## Leaving Facebook

That's right, folks. I'm leaving Facebook. This time I'm serious. My [author page](https://facebook.com/matthewgrayboschnovelist) is scheduled for deletion in 14 days. I'll be shutting down [my personal account](https://facebook.com/matthew.graybosch) on 31 December 2017.

Why? Because Facebook sucks. It has always been a shithole, but since it demonstrably fails to help me sell books there's no reason for me to stick around.

# 21 October 2017

## "Let It Die" by Survive Said the Prophet

*Let It Die* is this kinda crappy free-to-play game for the PS4 created by the outfit that bought out Grasshopper Manufacture, and features developer Suda51's trademark and profoundly fucked up sense of humor, but one interesting aspect of the game was its soundtrack, which you could access in game. Every song was called "Let it Die", but was written and performed by a different Japanese band.

One of the best of them was done by a band called [Survive Said the Prophet](http://survivesaidtheprophet.com/). Here's [the video](https://www.youtube.com/watch?v=UrOXTADrIjw).

<iframe width="560" height="315" src="https://www.youtube.com/embed/UrOXTADrIjw" frameborder="0" allowfullscreen></iframe>

## Scene 2 from Chapter 1 of *Shattered Guardian*

Let's see if I can get this scene finished. Maybe Naomi's entry into the Murdoch
building and subsequent confrontation with Imaginos and then Ashtoreth should be
a third scene.

In any case I've got plenty of time to get it right. I'm sitting at Barnes &
Noble at a table that's close to a power outlet, I've got a playlist with over a
thousand tracks, and my wireless headphones are at max charge.

Let's rock.

# 20 October 2017

## Start Page for suckless.org's Surf Browser

I've got a little [start page](/mewnix/surfstart/) that I originally
intended to use privately with the [Surf browser](http://surf.suckless.org) from
[suckless.org](http://suckless.org). Instead, I'll share it with everybody.

If you're a *person* and you have an actual website, [I want to know about
it](mailto:public@matthewgraybosch.com). I especially want to hear from
artists, writers, and musicians. 

But don't email me with business or corporate websites, or links to your social
media profiles. If you're not a person with your own website, then this isn't
about you.

## We're on Github

This new version of my website is now available on Github as [pandoc.matthewgraybosch.com](https://github.com/matthewgraybosch/pandoc.matthewgraybosch.com). When it's ready, it'll still go to [matthewgraybosch.com](https://matthewgraybosch.com).

# 19 October 2017

## Dena Helmi

I found this indie artist from DeviantArt named [Dena
Helmi](https://denahelmi.deviantart.com/) on Reddit this morning. Rather, she
posted this sketch in r/fantasy.

![B/W Elf Girl Sketch by Dena Helmi](/assets/images/denahelmi-elfgirl-bw.png)

I don't care much about elves, but I've been a fan of black-and-white with a
single color ever since I first saw it in the Robert Rodriguez adaptation of
Frank Miller's *Sin City* comics.

And then there's this.

![Sarina by Dena Helmi](/assets/images/sarina_by_denahelmi.png)

It's not quite the same, but I wouldn't mind seeing what Ms. Helmi might do with
some of my cast.

## White Identity Considered Harmful

Richard Spencer thinks race is the core of identity. I think he's a Nazi and an
ignorant shitfountain, but I repeat myself.

[More of this bullshit](/considered-harmful/white-identity/).

# 18 October 2017

## Pandoc

Looks like I've figured out how to use [pandoc](http://pandoc.org/) as a static
website generator. A bit of rudimentary shell scripting will let me integrate it
into a build/deploy workflow that will let me convert a collection of text files
formatted with [Markdown](https://daringfireball.net/projects/markdown/) into a
static HTML5 website.

Why pandoc? It seems pretty simple to me. 

[Jekyll](https://jekyllrb.com/) doesn't work on
[OpenBSD](https://www.openbsd.org/). It seems the OS likes to clamp down on how
many files I can have open at a time, and Jekyll opens too many at once when
building my site.

Don't worry if none of this makes sense to you. I'm just yak-shaving.

## Fun with HTTP servers

OK, maybe I need to RTFM again, but I haven't been able to make OpenBSD's
built-in httpd web server work for me. All I want to do is test my site locally,
dammit.

Oh, well. [darkhttpd](https://unix4lyfe.org/darkhttpd/) to the rescue. It claims
to listen on 0.0.0.0:8080 when you start it up and point it to the directory
containing your website, but don't be fooled. 127.0.0.1:8080 is where you want
to go.

## Badass Women with Swords

Yeah, that's what I'm into. 

!["She looks angry" by BH](/assets/images/b-h-knight-g.jpg)

!["Something's gonna happen" by BH](/assets/images/b-h-knight-g2.jpg)

This one kinda reminds me of Celes from *Final Fantasy VI* in a setting similar to *Dark Souls*. Artwork is by [BH on ArtStation](https://www.artstation.com/artwork/x6x21).

![Galvana Commission by Jackie Felix](/assets/images/jackie-felix-zz-knight-hammer-commission-v2-up.jpg)

And here's one with snow-blonde hair like Naomi from [Starbreaker](/starbreaker/) -- except that Nims still has both her eyes and favors a sword rather than an oversized hammer. Another [ArtStation](https://www.artstation.com/artwork/xKWYm) find.

## New Pages

Lots of new pages:

* [the home page](/)
* [this page](/news/)

## OpenBSD Screenshots and Dotfiles

Thinking of using [OpenBSD](http://openbsd.org) on your desktop or laptop? I've got some [configuration files](https://github.com/matthewgraybosch/desktop-openbsd-starter-kit) you might find useful. Also, here are some screenshots.

![Nothing but XConsole](/assets/images/openbsd-imaginos-20171018-clean.png)

![Modifying my window mangler's config file in Emacs](/assets/images/openbsd-imaginos-20171018-emacs.png)

![You can't post this to r/unixporn without a shoutout](/assets/images/openbsd-imaginos-20171018-seamonkey-reddit-unixporn.png)

![Just some info about my system in a terminal](/assets/images/openbsd-imaginos-20171018-urxvt-screenfetch.png)

It's not that hard to set up once you know what you're doing, but learning enough to be able to say you know what you're doing takes a bit of time and a lot of reading.
