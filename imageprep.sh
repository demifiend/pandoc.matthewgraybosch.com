#!/usr/bin/env bash

# NEVER RUN THIS AS PART OF THE BUILD PROCESS ON TRAVIS CI! 
# IT TAKES TOO BLOODY LONG AND CAUSES TIMEOUTS.
# RUN THIS SCRIPT LOCALLY, COMMIT, AND PUSH TO MASTER.

# We need to resize and compress images for display. 
# Script based on "Efficient Image Resizing With ImageMagick" by Dave Newton
# https://www.smashingmagazine.com/2015/06/efficient-image-resizing-with-imagemagick/

function processJPG {
  for img in ${1}*.jpg
  do
    jpegoptim --size=200k --all-progressive --strip-all $img;
  done
}

function processPNG {
  for img in ${1}*.png
  do
    pngcrush -d ${1} -brute -reduce $img;
  done
}

echo "STARTED image processing..."

export INPUT_PATH=./images/incoming/
export ORIGINALS=./images/originals/
export OUTPUT_PATH=./assets/images/
export OUTPUT_WIDTH=960

echo "Copying images to processing directories..."
cp ${INPUT_PATH}* $OUTPUT_PATH
mv ${INPUT_PATH}* $ORIGINALS

echo "Resizing images..."
processJPG $OUTPUT_PATH $OUTPUT_WIDTH;
processPNG $OUTPUT_PATH $OUTPUT_WIDTH;

echo "FINISHED image processing..."
