---
title: "Shattered Guardian: Part II of Starbreaker"
subtitle: |
  I'm an author, a software developer, a long-haired metalhead, and 
  an incorrigible bookworm with delusions of erudition.
lang: "en"
keywords:
  - author
  - science fiction
  - fantasy
  - cyberpunk
  - gonzo
  - writing
  - heavy metal
  - metalhead
  - Unix
  - Linux
  - OpenBSD
  - rants
  - homepage
  - Starbreaker
---

