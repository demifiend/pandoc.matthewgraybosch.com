--- 
title: "Everything Considered Harmful" 
subtitle: | 
  In which I inveigh with my tongue at least partway in my cheek 
  about everything that makes the world worth destroying. If I 
  write enough of these I might release them as a book.
lang: "en"
keywords:
  - considered harmful
  - rants
  - essays
  - everything that sucks
  - criticism
  - social issues
  - opinions
breadcrumbs:
  - name: Home
    url: /
...

# Why "Considered Harmful"

Why call this collection of rants "Considered Harmful"? Simple. I code for a
living. Though I dropped out of college, I stayed in school long enough to pick
up the rudiments of a computer science education, where I was exposed to quaint
concepts like "structured programming". 

This obvious-in-hindsight concept was one Edsger Dijkstra advocated in his 1968
letter to the ACM, published under the title *Go To Statement Considered
Harmful*. The title wasn't his idea.

Because techies are no more original than any other sort of artist, the phrase
continues to crop up in articles, essays, and rants attacking whatever trend the
author considers worrisome. I merely partake of a venerable, if obscure,
tradition.

# The Rants

These are posted in rough chronological order. Don't be surprised if I create a
file, dump a couple of paragraphs in it, and then come back later and write
more. This isn't a blog, either, so don't expect a schedule or for any of these
to ever be "finished".

[White Identity Considered Harmful](/considered-harmful/white-identity/)

: Richard Spencer thinks race is the core of identity. I think he's a Nazi and
  an ignorant shitfountain, but I repeat myself.
