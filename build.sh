#!/usr/bin/env bash

# Matthew Graybosch's pandoc website build script
# 2017, Matthew Graybosch
# Based on Will Styler's 2013 build script
# http://savethevowels.org/posts/lmimg/spcv.txt

echo "Cleaning out source directory."
find . -name \*.html -type f -exec rm {} \;
find . -name \*.css -type f -exec rm {} \;

echo "Building site."
sass ./assets/css/main.scss ./assets/css/main.css
find . -name \*.md -type f -exec pandoc -s -S --section-divs --email-obfuscation=references --template "./assets/layouts/_standard" --css "./assets/css/main.css" -f markdown+definition_lists+yaml_metadata_block+implicit_figures -t html5 -o {}.html {} \;
find . -depth -name '*.md.html' -execdir bash -c 'mv -i "$1" "${1//md.html/html}"' bash {} \;

echo "Copying site to local directory."
rsync -azvhCL --exclude=.DS_Store --exclude=images/incoming* --exclude=images/originals/* --exclude=*.html5 --exclude=*.scss --exclude=*.sh --exclude=*.md --exclude=*/.DS_Store --exclude=*/_archived/* --delete /home/demifiend/documents/pandoc.matthewgraybosch.com/* /home/demifiend/public_html/


