---
title: "About Matthew Graybosch"
subtitle: |
  Are you sure you want to know more about me? Fine. Here's 
  some biographical information suitable for public consumption.
lang: "en"
keywords:
  - bio
  - biography
  - about
  - press kit
  - photos
  - sample interview questions
  - excerpts
breadcrumbs:
  - name: Home
    url: /
---
So, do you want the short version or the long one?

# About Matthew: The Short Version

Matthew is the author of *Silent Clarion* and *Without Bloodshed*. He's supposed to be writing a sequel to *Without Bloodshed*, and he might actually finish it sometime before Ragnarok...

He's also a cat whisperer, a self-taught software developer, and his Google
searches are lunch-break conversations in government intelligence agencies
around the world. You may have heard of him.

![A photo of Matthew Graybosch at Niagara Falls by Catherine Gatt](/assets/images/author-niagarafalls.jpg)

# About Matthew: The Long Version

According to official records maintained by the state of New York, Matthew was born somewhere on Long Island in 1978.

Urban legends suggest he might be Rosemary’s Baby or the result of top-secret DOD attempts to continue Nazi experiments combining human technology and black magic. The most outlandish tale suggests that he sprang fully grown from his father’s forehead with a sledgehammer in one hand and the second edition of *The C Programming Language* in the other&mdash;and has been giving the poor man headaches ever since.

The truth is more prosaic. Matthew is an author from New York who lives with his wife and cats in central Pennsylvania. He is also an avid reader, a long-haired metalhead, and an unrepentant nerd with delusions of erudition.

*Without Bloodshed* (2013) is his first published novel, followed by *Silent Clarion* (2016). He has also written short stories like "The Milgram Battery", "Limited Liability", and "Tattoo Vampire". He is allegedly working on the next Starbreaker novel, *Shattered Guardian*, but sources within the NSA suggest he's doing nothing of the sort.

His day job is software development, and we're not sure how he remains sane. We could ask, but we suspect he’d say, "I'm not sane. I’m high-functioning."

![A photo of Matthew Graybosch in NYC by Catherine Gatt](/assets/images/author-nyc.jpg)
