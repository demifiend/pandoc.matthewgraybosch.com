---
title: "Why I Write; Or, the World's Shortest Manifesto"
subtitle: |
  I occasionally get asked why I write. That's the sort of question 
  that calls for a manifesto.
lang: "en"
keywords:
  - writing
  - reasons for writing
  - manifesto
breadcrumbs:
  - name: Home
    url: /
---
Why do I write? Here are some answers for you if you're honestly curious.

*   **I do it because I can.**
*   **I do it because I want to.**
*   **I do it because I *choose* to.**

These are the only reasons I need, but I could come up with others 
if necessary.

If you're asking in order to imply that I shouldn't write, here's the 
only answer you deserve.

* **Why? Because fuck you is why.**

The fundamental truth is that the stories I wanted to see told went 
untold, and I wasn't satisfied with what I had been reading. Rather 
than complaining about it, I decided to sit my ass down and *do* 
something about it.

What were you expecting, a manifesto?
