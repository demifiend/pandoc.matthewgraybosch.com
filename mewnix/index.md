--- 
title: "Mewnix" 
subtitle: | 
  I kept hearing that Unix was hard to use, but it turned
  out I could teach my cats to use it. They just can't 
  pronounce its name.
lang: "en"
keywords:
  - Unix
  - OpenBSD
  - Linux
  - free software
  - open source
  - configuration
  - screenshots
  - tools
breadcrumbs:
  - name: Home
    url: /
---

In any case, here's a collection of resources for users of Unix, Linux, and
free/open source software. You'll find screenshots, configuration files, HOWTOs
and other goodies.

* [start page for suckless.org's Surf browser](/mewnix/surfstart/) 
