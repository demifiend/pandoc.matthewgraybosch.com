--- 
title: "surf start page" 
subtitle: | 
  I use this as a starting page for the Surf browser from suckless.org.
  You're welcome to do the same, if you like. Just save a copy, or link here.
lang: "en"
keywords:
  - news
breadcrumbs:
  - name: Home
    url: /
  - name: Mewnix
    url: /mewnix/
---

Are you a *real* human being, and not a corporation or a brand? Do you have your
own website on your own domain? [Tell me about
it](mailto:public@matthewgraybosch.com). If it's interesting, I'll link to it
here.

# My Websites

* [Localhost](http://127.0.0.1:8080)
* [matthewgraybosch.com](https://matthewgraybosch.com)
* [starbreakersaga.com](https://starbreakersaga.com)

# Search Engines

* [DuckDuckGo](https://duckduckgo.com)
* [StartPage (uses Google)](https://startpage.com/)

# People

## Authors

* [Matthew Cox](http://www.matthewcoxbooks.com/wordpress/)
* [James Wymore](https://jameswymore.wordpress.com/)
* [Lisa Janice Cohen](http://ljcohen.net/)

## Fans

* [Kathryn Huxtable](http://www.kathrynhuxtable.org/blog/)

# News/Propaganda

All news is fake news, but some is less fake.

## Wire Services

* [Associated Press News](https://www.apnews.com/)
* [Reuters News](https://www.reuters.com/)

## Weird/Silly News

* [FARK](https://fark.com)

## Tech Blogs/Aggregators

* [AllSides](https://allsides.com)
* [BoingBoing](https://boingboing.net/)
* [Hacker News](https://news.ycombinator.com/)
* [Slashdot](https://slashdot.org)

## Leftist Sources

* [The Guardian](https://guardian.co.uk)
* [World Socialist Web
  Site](https://www.wsws.org/en/topics/site_area/news/)
* [Jacobin](https://jacobinmag.com)
* [The Baffler](https://thebaffler.com)

## Foreign Sources

* [The BBC](https://bbc.co.uk)
* [Al-Jazeera English](http://www.aljazeera.com/news/)
